
const exceptions =
      [{"nominative": { first: "Дмитрий", middle: "Владимирович", last: "Борок" },
        "genitive": { first: "Дмитрия", middle: "Владимировича", last: "Борока" }}];

function inclineWithExceptions(fi) {
    const fie = exceptions.find(({ nominative }) => nominative.first === fi.first && nominative.middle === fi.middle && nominative.last === fi.last);
    return fie ? fie.genitive : lvovich.incline(fi, 'genitive');
}

$(function(){
    $.blockUI({ message: "<h1>Converting to genitive...</h1>" });
    $('.captain').each(function(){
        var res = /.*капитан (.*) (.*) (.*)\).*/.exec(this.innerHTML);
        var fi = { first: res[1], middle: res[2], last: res[3] };
        var fig = inclineWithExceptions(fi);
        this.innerHTML = fig.first + ' ' + fig.last;
    });
    $.unblockUI();
});

