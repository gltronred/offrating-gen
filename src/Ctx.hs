{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StrictData #-}

module Ctx where

import           Common
import           Log
import           Release (ReleaseItem(..),Place(..),Buildup(..))
import           Types

import RatingChgkInfo ( RatingClient
                      , runRatingApi
                      , getAllItems
                      , TeamBaseRecap(..)
                      , SeasonMap(..)
                      , teamBaseRecaps
                      , Player(..)
                      , players
                      , Team(..)
                      , teams
                      , TournamentShort(..)
                      , tournaments
                      , Tournament(..)
                      , tournament
                      , RecapTeam(..)
                      , RecapPlayer(..)
                      , tournamentRecaps
                      , TournamentResult(..)
                      , tournamentResults
                      )
import           RatingChgkInfo.Types.Unsafe ( TeamId(..)
                                             , PlayerId(..)
                                             , TournamentId(..)
                                             )

import           Data.Aeson
import           Data.IntMap.Strict (IntMap, (!), (!?))
import qualified Data.IntMap.Strict as M
import           Data.List (groupBy)
import qualified Data.Map.Strict as Map
import qualified Data.Text as T
import           Data.Time (Day, localDay)
import           System.Directory (listDirectory)
import           System.IO (hPutStrLn, stderr)

toPlayerCtx :: Maybe Flag -> Player -> PlayerCtx
toPlayerCtx mflag player = let
  ident = playerId player
  n = Name (name player) (patronymic player) (surname player)
  in PlayerCtx (fromFullId ident) n mflag

toCaptain :: Player -> Captain
toCaptain p = Captain (pcName $ toPlayerCtx Nothing p) Nothing

toBaseRecapCtx :: DB -> TeamBaseRecap -> BaseRecapCtx
toBaseRecapCtx (playerDb,_,_) base = BaseRecapCtx
  { brcSeasonId = tbr_idseason base
  , brcSeason = "<Нужно соответствие сезонов годам>" -- TODO: fix
  , brcPlayers = map (toPlayerCtx Nothing . (playerDb !)) $
                 mapMaybe (readMaybe . T.unpack) $
                 tbr_players base
  }

getTeamCtx :: Int -> DB -> [TournamentCtx] -> ReleaseItem -> IO TeamCtx
getTeamCtx seasonId db@(playerDb,teamDb,tourDb) tourCtx ri = do
  let ident = riId ri
      teamId = TeamId $ T.pack $ show ident
      team = teamDb ! ident
  recaps <- log ("Get team" ++ show ident) $
    rr [] $ Map.toDescList . unSeasonMap <$> teamBaseRecaps teamId
  let tours = reverse $
              sortBy (compare`on`tiDateEnd.ttcInfo) $
              concatMap (filter ((==ident) . full . ttcTeamId) . trcTeams) tourCtx
      allPlayers = sortBy (comparing $ negate . snd) $
                   map ((\(Just g) -> (head g, length g)).nonEmpty) $
                   groupBy ((==)`on`pcId) $
                   sortBy (comparing $ full . pcId) $
                   concatMap ttcRecap tours
      lastRecap = case recaps of
        [] -> Nothing
        ((s,r):_)
          | s == seasonId -> Just r
          | otherwise -> Nothing
      cptId = lastRecap >>= \rs -> case tbr_captain rs of
        String s -> readMaybe $ T.unpack s
        _ -> Nothing
      pids = case lastRecap of
        Just rs -> mapMaybe (readMaybe . T.unpack) $ tbr_players rs
        Nothing -> case take 6 $ map (full.pcId.fst) allPlayers of
          ps@(_:_) -> ps
          [] -> case recaps of
            (s,r) : _ -> mapMaybe (readMaybe . T.unpack) $ tbr_players r
            [] -> []
      players = mapMaybe (playerDb !?) pids
      cpt = case cptId of
        Just ident -> Just $ playerDb ! ident
        Nothing -> case players of
          p:_ -> Just p
          [] -> Nothing
      Place position = riPlace ri
  pure $ TeamCtx
    { tcId = fromFullId ident
    , tcCaptain = toCaptain <$> cpt
    , tcTown = fromMaybe "<Неизвестный>" $ tm_town team
    , tcPosition = position
    , tcRating = riRating ri
    , tcBuildup = riBuildup ri == Buildup
    , tcLastRecap = map (toPlayerCtx Nothing) players
    , tcBaseRecap = map (toBaseRecapCtx db) $ map snd recaps
    , tcTournaments = tours
    }

toBuildupCtx :: [TournamentCtx] -> Team -> BuildupCtx
toBuildupCtx tourCtx team = let
  teamId = tm_idteam team
  ident = ratingId unTeamId id teamId
  tours = concatMap (filter ((==ident) . full . ttcTeamId) . trcTeams) tourCtx
  in BuildupCtx
     { bcId = fromFullId ident
     , bcTown = fromMaybe "<Неизвестный>" $ tm_town team
     , bcTournaments = tours
     }

getReleaseCtx :: Int -> DB -> Day -> [TournamentCtx] -> [ReleaseItem] -> IO ReleaseCtx
getReleaseCtx season db day tourCtx = fmap (fmap $ ReleaseCtx day) $
                                      mapM $ getTeamCtx season db tourCtx

getReleaseListCtx :: IO ReleaseListCtx
getReleaseListCtx = do
  fs <- reverse . sort . filter ((=="20") . take 2) <$> listDirectory "."
  let ds = mapMaybe readMaybe fs
  case ds of
    [] -> die "Releases not found"
    lastRelease : _ -> pure $ ReleaseListCtx
      { rcDate = lastRelease
      , rcReleases = ds
      }

toTourTeamCtx :: DB -> TourInfo -> Int -> RecapTeam -> TournamentResult -> TournamentTeamCtx
toTourTeamCtx db@(playerDb,teamDb,tourDb) info ident
  RecapTeam{rt_recaps=recap} result = let
  mcptId = case filter ((=="1").rp_is_captain) recap of
    c:_ -> Just $ ratingId unPlayerId id $ rp_idplayer c
    [] -> listToMaybe $ map (ratingId unPlayerId rp_idplayer) recap
  cpt = toCaptain . (playerDb !) <$> mcptId
  toFlag RecapPlayer{rp_is_captain=c,rp_is_base=b,rp_is_foreign=f} =
    case (c,b,f) of
      ("1",_,_) -> Just CaptainFlag
      ("0","1","0") -> Just BaseFlag
      ("0","0","1") -> Just LegFlag
      (_,_,_) -> Nothing
  toRecapPlayerCtx r = toPlayerCtx (toFlag r) <$>
                       playerDb !? (ratingId unPlayerId rp_idplayer r)
  recapCtx = mapMaybe toRecapPlayerCtx recap
  in TournamentTeamCtx
     { ttcTeamId = fromFullId ident
     , ttcCaptain = cpt
     , ttcTown = fromMaybe "<Неизвестный>" $ tm_town $ teamDb ! ident
     , ttcBuildup = tr_included_in_rating result == 0
     , ttcRecap = recapCtx
     , ttcResult = tr_mask result
     , ttcCount = fromMaybe (negate 1) $ readMaybe $ T.unpack $
                  tr_questions_total result
     , ttcPosition = fromMaybe (negate 9999) $ readMaybe $ T.unpack $
                     tr_position result
     , ttcInfo = info
     }

getTournamentCtx :: DB -> Int -> IO TournamentCtx
getTournamentCtx db@(playerDb, teamDb, tourDb) ident = do
  let tourId = TournamentId $ T.pack $ show ident
      tour = tourDb ! ident
  tourFull <- rr Nothing $ listToMaybe <$> tournament tourId
  recaps <- fmap (toMap $ ratingId unTeamId rt_idteam) $
            rr [] $ tournamentRecaps tourId
  results <- fmap (toMap $ ratingId unTeamId tr_idteam) $
             rr [] $ tournamentResults tourId
  let info = TourInfo
        { tiId = fromFullId ident
        , tiName = trs_name tour
        , tiDateStart = localDay $ trs_dateStart tour
        , tiDateEnd = localDay $ trs_dateEnd tour
        }
      teams = M.intersectionWithKey (toTourTeamCtx db info) recaps results
  pure $ TournamentCtx
    { trcInfo = info
    , trcFull = tourFull
    , trcTeams = sortBy (compare`on`ttcPosition) $ M.elems teams
    }

