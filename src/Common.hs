module Common where

import           Log
import           Types

import RatingChgkInfo ( RatingClient
                      , runRatingApi
                      , getAllItems
                      , Player(..)
                      , players
                      , Team(..)
                      , teams
                      , TournamentShort(..)
                      , tournaments
                      )
import           RatingChgkInfo.Types.Unsafe ( TeamId(..)
                                             , PlayerId(..)
                                             , TournamentId(..)
                                             )

import qualified Data.IntMap as M
import qualified Data.Text as T
import           System.IO (hPutStrLn, stderr)

rr :: a -> RatingClient a -> IO a
rr def act = do
  er <- runRatingApi act
  case er of
    Left err -> do
      hPutStrLn stderr $ show err
      pure def
    Right r -> pure r

ratingId :: (b -> Text) -> (a -> b) -> a -> Int
ratingId unF f = fromMaybe 0 . readMaybe . T.unpack . unF . f

playerId :: Player -> Int
playerId = ratingId unPlayerId idplayer

teamId :: Team -> Int
teamId = ratingId unTeamId tm_idteam

tournamentId :: TournamentShort -> Int
tournamentId = ratingId unTournamentId trs_idtournament

withDebugId :: (Maybe Int -> RatingClient a) -> Maybe Int -> RatingClient a
withDebugId act mp = do
  liftIO $ print mp
  act mp

toMap :: (a -> Int) -> [a] -> IntMap a
toMap f = M.fromList . map (\a -> (f a, a))

getPlayers :: IO (IntMap Player)
getPlayers = log "Get players" $
             fmap (toMap playerId) $
             rr [] $ getAllItems $ withDebugId players

getTeams :: IO (IntMap Team)
getTeams = log "Get teams" $
           fmap (toMap teamId) $
           rr [] $ getAllItems $ withDebugId teams

getTournaments :: IO (IntMap TournamentShort)
getTournaments = log "Get tournaments" $
                 fmap (toMap tournamentId) $
                 rr [] $ getAllItems $ withDebugId tournaments


