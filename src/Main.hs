{-
Layout:

/index.html    ->  YYYY-MM-DD/index.html
/releases.html ->  YYYY-MM-DD/releases.html
/YYYY-MM-DD/index.html
/YYYY-MM-DD/releases.html
/YYYY-MM-DD/team/XY/NNN.html
/YYYY-MM-DD/tour/AB/MMM.html
/...

Better layout:
/YYYY-MM-DD/index.html
/YYYY-MM-DD/releases.html
/team/XY/NNN.html
/tour/AB/MMM.html

-}

{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}

module Main where

import Common
import Ctx
import Db
import Log
import Pages
import Release (ReleaseItem(..), getRelease)
import Types

import RatingChgkInfo (Player, TournamentShort(..), Tournament(trn_tournamentInRating))

import Data.ByteString.Char8 (unpack, stripPrefix, stripSuffix)
import qualified Data.IntMap as M
import qualified Data.IntSet as S
import Data.Text (pack)
import Data.Time
import Options.Generic
import System.Directory
import System.Environment
import System.IO (hPutStr, hPutStrLn, hFlush)

{-
release <id>
team <id>
tournaments
tournament <id>
-}

data Mode
  = MakeAll
    { rootDir :: String <?> "Root directory for result"
    , releaseId :: Int  <?> "Release identifier"
    , season :: Int     <?> "Last season (for base recaps)"
    , count :: Maybe Int<?> "Show only first <count> teams"
    , pages :: Maybe Int<?> "Paginate using <pages> items"
    , begin :: Maybe Day<?> "Tournaments ending after this day"
    , end :: Maybe Day  <?> "Tournaments ending before this day"
    }
  | Database
    { rootDir :: String <?> "Root directory for result"
    }
  | Release
    { rootDir :: String <?> "Root directory for result"
    , releaseId :: Int  <?> "Release identifier"
    }
  | Teams
    { rootDir :: String <?> "Root directory for result"
    , day :: Day        <?> "Release day"
    , season :: Int     <?> "Last season (for base recaps)"
    , count :: Maybe Int<?> "Show only first <count> teams"
    , pages :: Maybe Int<?> "Paginate using <pages> items"
    }
  | Tournaments
    { rootDir :: String <?> "Root directory for result"
    , begin :: Maybe Day<?> "Tournaments ending after this day"
    , end :: Maybe Day  <?> "Tournaments ending before this day"
    , pages :: Maybe Int<?> "Paginate using <pages> items"
    }
  | ReleaseList
    { rootDir :: String <?> "Root directory for result"
    }
  | LinkIndex
    { rootDir :: String <?> "Root directory for result"
    , day :: Day        <?> "Release day"
    }
  deriving (Show,Generic)
instance ParseRecord Mode

main :: IO ()
main = do
  mode <- getRecord "Offline rating generator"
  wd <- getCurrentDirectory
  let tpls = wd ++ "/templates"
  withCurrentDirectory (unHelpful $ rootDir mode) $ case mode of
    MakeAll _ (Helpful ident) (Helpful season) (Helpful mcnt) (Helpful mpages) (Helpful mbegin) (Helpful mend) -> do
      db <- getSaveDb
      _newTours <- prepareTournaments (Just db) tpls mbegin mend $ fromMaybe 100 mpages
      (relDay, release) <- prepareRelease ident
      mtours <- loadTourCtx
      prepareTeams (Just db) relDay (Just release) mtours tpls season mcnt $ fromMaybe 100 mpages
      prepareReleaseList tpls
      linkIndex tpls relDay
    Database _ -> void getSaveDb
    Tournaments _ (Helpful begin) (Helpful end) (Helpful mpages) -> do
      mdb <- loadDb
      void $ prepareTournaments mdb tpls begin end $ fromMaybe 100 mpages
    Release _ (Helpful ident) -> do
      (relDay, _release) <- prepareRelease ident
      hPutStrLn stderr $ show relDay
    Teams _ (Helpful relDay) (Helpful season) (Helpful mcnt) (Helpful mpages) -> do
      mdb <- loadDb
      mrelease <- loadRelease relDay
      mtours <- loadTourCtx
      prepareTeams mdb relDay mrelease mtours tpls season mcnt $ fromMaybe 100 mpages
    ReleaseList _ -> do
      prepareReleaseList tpls
    LinkIndex _ (Helpful relDay) -> linkIndex tpls relDay

prepareRelease :: Int -> IO (Day, [ReleaseItem])
prepareRelease ident = do
  erel <- log ("Get release " ++ show ident) $
    getRelease True ident
  case erel of
    Left err -> die ("Something wrong with release: " ++ unpack err) >> pure (error "Not used")
    Right (fname, release) -> case stripPrefix "release-" fname of
      Nothing -> die ("Wrong release file: " ++ unpack fname) >> pure (error "Not used")
      Just s -> case stripSuffix ".csv" s >>= readMaybe . unpack of
        Nothing -> die ("Wrong release file: " ++ unpack fname) >> pure (error "Not used")
        Just date -> do
          saveRelease date release
          pure (date, release)

prepareTeams :: Maybe DB -> Day -> Maybe [ReleaseItem] -> Maybe [TournamentCtx] -> FilePath -> Int -> Maybe Int -> Int -> IO ()
prepareTeams mdb relDay mrelease mtourCtx tpls season mcnt pages = do
  case (mdb, mrelease, mtourCtx) of
    (Nothing, _, _) -> die "Please, prepare db before teams"
    (Just _, Nothing, _) -> die "Please, prepare release before teams"
    (Just _, Just _, Nothing) -> die "Please, prepare tournaments before teams"
    (Just db@(_,teamDb,_), Just fullRelease, Just tourCtx) -> do
      let release = case mcnt of
            Nothing -> fullRelease
            Just cnt -> take cnt fullRelease
      context <- getReleaseCtx season db relDay tourCtx release
      log "Render index" $ do
        let mkPage lst = Page
              { pageTitle = pack $ "Релиз " ++ show relDay
              , pageNav = NavCtx True False False
              , pagePage = Nothing
              , pageContent = context { mcTeams = lst }
              }
        renderWithPagination tpls "index" (show relDay) "index" mkPage pages $ mcTeams context
      let regularIds = S.fromList $ map (full . tcId) $ mcTeams context
      log "Render buildups" $ forM_ teamDb $ \team -> when (teamId team `S.notMember` regularIds) $ do
        let buildupCtx = toBuildupCtx tourCtx team
            ident = bcId buildupCtx
            title = pack $ "Сборная " ++ show (full ident)
            fname = tcId
            teamPage = Page
              { pageTitle = title
              , pageNav = NavCtx False False False
              , pagePage = Nothing
              , pageContent = buildupCtx
              }
        renderPage tpls "buildup" ("team/" ++ dir ident) (page ident ++ ".html") teamPage
      log "Render teams" $ forM_ (mcTeams context) $ \teamCtx -> do
        let ident = tcId teamCtx
            title = pack $ "Команда " ++ show (full ident)
            fname = tcId
            teamPage = Page
              { pageTitle = title
              , pageNav = NavCtx False False False
              , pagePage = Nothing
              , pageContent = teamCtx
              }
        renderPage tpls "team" ("team/" ++ dir ident) (page ident ++ ".html") teamPage

prepareTournaments :: Maybe DB -> FilePath -> Maybe Day -> Maybe Day -> Int -> IO [TournamentCtx]
prepareTournaments mdb tpls begin end pages = do
  case mdb of
    Nothing -> die "Please, prepare db before tournaments"
    Just db@(_,_,tourDb) -> do
      let beginP = maybe (const True) (\b t -> localDay (trs_dateEnd t) >= b) begin
          endP = maybe (const True) (\e t -> localDay (trs_dateEnd t) < e) end
          tours = M.toList $ M.filter (\t -> beginP t && endP t) tourDb
      newCtx <- forM tours $ \(tourId, _) -> prepareTour tpls db tourId
      context <- fmap (reverse .
                       sortBy (compare`on`tiDateEnd.trcInfo) .
                       filter ((==Just "1").fmap trn_tournamentInRating.trcFull)) $
                 appendTourCtx newCtx
      let mkPage lst = Page
            { pageTitle = "Турниры"
            , pageNav = NavCtx False False True
            , pagePage = Nothing
            , pageContent = lst
            }
      renderWithPagination tpls "tournaments" "." "tournaments" mkPage pages context
      pure context

prepareTour :: FilePath -> DB -> Int -> IO TournamentCtx
prepareTour tpls db tourId = do
  context <- log ("Get tour " ++ show tourId) $
             getTournamentCtx db tourId
  when (fmap trn_tournamentInRating (trcFull context) == Just "1") $ do
    let ident = fromFullId tourId
        tourPage = Page
          { pageTitle = tiName $ trcInfo context
          , pageNav = NavCtx False False False
          , pagePage = Nothing
          , pageContent = context
          }
    renderPage tpls "tournament" ("tour/" ++ dir ident) (page ident ++ ".html") tourPage
  pure context

prepareReleaseList :: FilePath -> IO ()
prepareReleaseList tpls = log "Prepare releases" $ do
  context <- getReleaseListCtx
  let relPage = Page
        { pageTitle = "Список релизов"
        , pageNav = NavCtx False True False
        , pagePage = Nothing
        , pageContent = context
        }
  renderPage tpls "releases" "." "releases.html" relPage

linkIndex :: String -> Day -> IO ()
linkIndex tpls relDay = log "Link indices" $ do
  -- wd <- getCurrentDirectory
  -- rels <- reverse . sort . mapMaybe readMaybe <$> listDirectory wd
  -- -- create dir && change to it
  -- createDirectoryIfMissing True date
  -- withCurrentDirectory date $ do
    -- renderPages tpls date rels teamsCtx
  -- link index
  -- whenM (doesPathExist "releases.html") $ removeFile "releases.html"
  -- createFileLink (date ++ "/releases.html") "releases.html"
  fs <- listDirectory "."
  forM_ (filter ((=="index") . take 5) fs) removeFile
  ns <- listDirectory $ show relDay
  forM_ (filter ((=="index") . take 5) ns) $ \f -> do
    createFileLink (show relDay ++ "/" ++ f) f

