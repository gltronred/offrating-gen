module Log where

import System.IO (hPutStr, hPutStrLn, hFlush, stderr)

log :: String -> IO a -> IO a
log s act = do
  hPutStr stderr $ s ++ "..."
  hFlush stderr
  a <- act
  hPutStrLn stderr "ok"
  pure a

