module Db where

import Prelude hiding (empty, fromList)

import Common
import Log
import Release (ReleaseItem(..),Place(..),Buildup(..))
import Types

import Data.IntMap (IntMap, elems, empty, fromList, union)
import Data.Aeson
import Data.Time

getSaveDb :: IO DB
getSaveDb = do
  ps <- getPlayers
  ts <- getTeams
  trs <- getTournaments
  encodeFile "context.json" (ps,ts,trs)
  pure (ps,ts,trs)

loadDb :: IO (Maybe DB)
loadDb = log "Load db" $ decodeFileStrict' "context.json"

saveRelease :: Day -> [ReleaseItem] -> IO ()
saveRelease date release = encodeFile ("release-" ++ show date ++ ".json") release

loadRelease :: Day -> IO (Maybe [ReleaseItem])
loadRelease relDay = decodeFileStrict' $ "release-" ++ show relDay ++ ".json"

loadTourCtxRaw :: IO (Maybe (IntMap TournamentCtx))
loadTourCtxRaw = decodeFileStrict' "tourCtx.json"

appendTourCtx :: [TournamentCtx] -> IO [TournamentCtx]
appendTourCtx context = do
  oldCtxMap <- fromMaybe empty <$> loadTourCtxRaw
  let newMap = fromList $ map (\tc -> (full $ tiId $ trcInfo tc, tc)) context
      fullCtx = union newMap oldCtxMap
  encodeFile "tourCtx.json" fullCtx
  pure $ elems fullCtx

loadTourCtx :: IO (Maybe [TournamentCtx])
loadTourCtx = log "Load tournaments" $ fmap elems <$> loadTourCtxRaw

