{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Types where

import RatingChgkInfo.Types (Player, Team, TournamentShort, Tournament)

import Data.Aeson
import Data.Aeson.Types
import Data.Char (toLower)
import Data.Text (Text)
import Data.Time (UTCTime, Day)
import GHC.Generics
import Text.Printf

type DB = (IntMap Player, IntMap Team, IntMap TournamentShort)

data Ident = Ident
  { full :: Int
  , dir :: String
  , page :: String
  } deriving (Eq,Show,Read,Generic)

instance FromJSON Ident
instance ToJSON Ident

fromFullId :: Int -> Ident
fromFullId ident = Ident ident folderStr pageStr
  where (folderId, pageId) = ident `divMod` 1000
        folderStr = printf "%03d" folderId
        pageStr = printf "%03d" pageId

data Name = Name
  { first :: Text
  , middle :: Maybe Text
  , last :: Text
  } deriving (Eq,Show,Read,Generic)

instance FromJSON Name
instance ToJSON Name

data Captain = Captain
  { nominative :: Name
  , genitive :: Maybe Name
  } deriving (Eq,Show,Read,Generic)

instance FromJSON Captain
instance ToJSON Captain

data BaseRecapCtx = BaseRecapCtx
  { brcSeasonId :: Int
  , brcSeason :: Text
  , brcPlayers :: [PlayerCtx]
  } deriving (Eq,Show,Read,Generic)

instance ToJSON BaseRecapCtx where
  toJSON = genericToJSON $ opts 3
  toEncoding = genericToEncoding $ opts 3

data TeamCtx = TeamCtx
  { tcId :: Ident
  , tcCaptain :: Maybe Captain
  , tcTown :: Text
  , tcPosition :: Double
  , tcRating :: Int
  , tcBuildup :: Bool
  , tcLastRecap :: [PlayerCtx]
  , tcBaseRecap :: [BaseRecapCtx]
  , tcTournaments :: [TournamentTeamCtx]
  } deriving (Eq,Show,Read,Generic)

instance ToJSON TeamCtx where
  toJSON = genericToJSON $ opts 2
  toEncoding = genericToEncoding $ opts 2

data BuildupCtx = BuildupCtx
  { bcId :: Ident
  --, bcCaptain :: Maybe Captain
  , bcTown :: Text
  --, bcPosition :: Double
  --, bcRating :: Int
  --, bcBuildup :: Bool
  --, bcLastRecap :: [PlayerCtx]
  --, bcBaseRecap :: [BaseRecapCtx]
  , bcTournaments :: [TournamentTeamCtx]
  } deriving (Eq,Show,Read,Generic)

instance ToJSON BuildupCtx where
  toJSON = genericToJSON $ opts 2
  toEncoding = genericToEncoding $ opts 2

data ReleaseCtx = ReleaseCtx
  { mcDate :: Day
  , mcTeams :: [TeamCtx]
  } deriving (Eq,Show,Read,Generic)

instance ToJSON ReleaseCtx where
  toJSON = genericToJSON $ opts 2
  toEncoding = genericToEncoding $ opts 2

data ReleaseListCtx = ReleaseListCtx
  { rcDate :: Day
  , rcReleases :: [Day]
  } deriving (Eq,Show,Read,Generic)

instance ToJSON ReleaseListCtx where
  toJSON = genericToJSON $ opts 2
  toEncoding = genericToEncoding $ opts 2

data Flag = CaptainFlag | BaseFlag | LegFlag
  deriving (Eq,Show,Read,Generic)

flagToText :: Flag -> Text
flagToText f = case f of
  CaptainFlag -> "К"
  BaseFlag -> "Б"
  LegFlag -> "Л"

instance FromJSON Flag where
  parseJSON (String s)
    | s == "К" = pure CaptainFlag
    | s == "Б" = pure BaseFlag
    | s == "Л" = pure LegFlag
    | otherwise = unexpected (String s)
  parseJSON v = unexpected v
instance ToJSON Flag where
  toJSON = toJSON . flagToText
  toEncoding = toEncoding . flagToText

data PlayerCtx = PlayerCtx
  { pcId :: Ident
  , pcName :: Name
  , pcFlag :: Maybe Flag
  } deriving (Eq,Show,Read,Generic)

instance FromJSON PlayerCtx where
  parseJSON = genericParseJSON $ opts 2
instance ToJSON PlayerCtx where
  toJSON = genericToJSON $ opts 2
  toEncoding = genericToEncoding $ opts 2

data PlayerListCtx = PlayerListCtx
  { plcPlayers :: [PlayerCtx]
  } deriving (Eq,Show,Read,Generic)

instance ToJSON PlayerListCtx where
  toJSON = genericToJSON $ opts 3
  toEncoding = genericToEncoding $ opts 3

data TourInfo = TourInfo
  { tiId :: Ident
  , tiName :: Text
  , tiDateStart :: Day
  , tiDateEnd :: Day
  } deriving (Eq,Show,Read,Generic)

instance FromJSON TourInfo where
  parseJSON = genericParseJSON $ opts 2
instance ToJSON TourInfo where
  toJSON = genericToJSON $ opts 2
  toEncoding = genericToEncoding $ opts 2

data TournamentCtx = TournamentCtx
  { trcInfo :: TourInfo
  , trcFull :: Maybe Tournament
  , trcTeams :: [TournamentTeamCtx]
  } deriving (Eq,Show,Read,Generic)

instance FromJSON TournamentCtx where
  parseJSON = genericParseJSON $ opts 3
instance ToJSON TournamentCtx where
  toJSON = genericToJSON $ opts 3
  toEncoding = genericToEncoding $ opts 3

data TournamentTeamCtx = TournamentTeamCtx
  { ttcTeamId :: Ident
  , ttcCaptain :: Maybe Captain
  , ttcTown :: Text
  , ttcBuildup :: Bool
  , ttcRecap :: [PlayerCtx]
  , ttcResult :: Text
  , ttcCount :: Int
  , ttcPosition :: Double
  , ttcInfo :: TourInfo
  } deriving (Eq,Show,Read,Generic)

instance FromJSON TournamentTeamCtx where
  parseJSON = genericParseJSON $ opts 3
instance ToJSON TournamentTeamCtx where
  toJSON = genericToJSON $ opts 3
  toEncoding = genericToEncoding $ opts 3

data TournamentListCtx = TournamentListCtx
  { tlcTournaments :: [TournamentCtx]
  } deriving (Eq,Show,Read,Generic)

instance ToJSON TournamentListCtx where
  toJSON = genericToJSON $ opts 3
  toEncoding = genericToEncoding $ opts 3

data NavCtx = NavCtx
  { navRoot :: Bool
  , navReleases :: Bool
  , navTournaments :: Bool
  } deriving (Eq,Show,Read,Generic)

instance ToJSON NavCtx where
  toJSON = genericToJSON $ opts 3
  toEncoding = genericToEncoding $ opts 3

data PageName = PageName
  { pnLink :: Maybe Int
  , pnText :: Int
  } deriving (Eq,Show,Read,Generic)

instance ToJSON PageName where
  toJSON = genericToJSON $ opts 2
  toEncoding = genericToEncoding $ opts 2

data PageCtx = PageCtx
  { pcPrefix :: String
  , pcSuffix :: String
  , pcFirst :: Maybe PageName
  , pcPrevious :: [PageName]
  , pcCurrent :: PageName
  , pcNext :: [PageName]
  , pcLast :: Maybe PageName
  } deriving (Eq,Show,Read,Generic)

instance ToJSON PageCtx where
  toJSON = genericToJSON $ opts 2
  toEncoding = genericToEncoding $ opts 2

data Page a = Page
  { pageTitle :: Text
  , pageNav :: NavCtx
  , pagePage :: Maybe PageCtx
  , pageContent :: a
  } deriving (Eq,Show,Read,Generic)

instance ToJSON a => ToJSON (Page a) where
  toJSON = genericToJSON $ opts 4
  toEncoding = genericToEncoding $ opts 4

opts :: Int -> Options
opts k = defaultOptions
  { fieldLabelModifier = firstLower . drop k }
  where firstLower [] = []
        firstLower (x:xs) = toLower x : xs

