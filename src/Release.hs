{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE TupleSections #-}

module Release where

import           Prelude hiding (ByteString, get)

import           Codec.Text.IConv
import           Control.Lens
import           Data.Aeson
import           Data.Aeson.Types (unexpected)
import           Data.ByteString.Char8 (ByteString)
import qualified Data.ByteString.Char8 as B
import qualified Data.ByteString.Lazy.Char8 as LB
import           Data.ByteString.Lazy.Search (replace)
import           Data.Csv
import           Data.Text (Text)
import qualified Data.Vector as V
import           Network.Wreq

data ReleaseItem = ReleaseItem
  { riId :: Int
  , riName :: ()                -- Not used in "official" rating
  , riTown :: Text
  , riPlace :: Place
  , riRating :: Int
  , riBuildup :: Buildup
  } deriving (Eq,Show,Read,Generic)
instance FromRecord ReleaseItem
instance FromJSON ReleaseItem
instance ToJSON ReleaseItem

newtype Place = Place Double deriving (Eq,Show,Read,Generic,FromJSON,ToJSON)

instance FromField Place where
  parseField s = Place <$> parseField (B.map (\c -> if c==',' then '.' else c) s)

data Buildup
  = Base Int
  | Buildup
  deriving (Eq,Show,Read,Generic)
instance FromJSON Buildup where
  parseJSON (Bool f) = pure Buildup
  parseJSON (Array xs) = Base <$> parseJSON (V.head xs)
  parseJSON v = unexpected v
instance ToJSON Buildup where
  toJSON Buildup = toJSON False
  toJSON (Base x) = toJSON [x]

instance FromField Buildup where
  parseField s = case runParser (parseField s) of
    Left err -> pure Buildup
    Right b -> pure $ Base b

getRelease :: Bool -> Int -> IO (Either ByteString (ByteString, [ReleaseItem]))
getRelease irregs ident = do
  let url = "https://rating.chgk.info/teams.php?download_data=export_release&release=" ++ show ident ++ (if irregs then "&with_irregulars=1" else "")
  r <- get url
  pure $ case r^.responseStatus.statusCode of
    200 -> case r^?responseHeader "Content-disposition" of
      Nothing -> Left "No header Content-disposition"
      Just disp -> do
        fname <- mkFile disp
        rel <- parseRelease $ r^.responseBody
        pure (fname, rel)
    _ -> Left $ r^.responseStatus.statusMessage

mkFile :: ByteString -> Either ByteString ByteString
mkFile disp = let
  signalString = "attachment;filename="
  (name, content) = B.splitAt (B.length signalString) disp
  in if name == signalString
     then pure content
     else Left $ "Wrong header Content-disposition: " `B.append` disp

parseRelease :: LB.ByteString -> Either ByteString [ReleaseItem]
parseRelease = chErr B.pack .
               fmap toList .
               decodeWith csvOpts HasHeader .
               LB.drop 3
  where chErr f (Left e) = Left $ f e
        chErr _ (Right r)= Right r

csvOpts :: DecodeOptions
csvOpts = defaultDecodeOptions
  { decDelimiter = fromIntegral $ ord ','
  }

