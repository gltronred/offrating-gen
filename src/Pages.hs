{-# LANGUAGE OverloadedStrings #-}

module Pages where

import           Ctx
import           Log (log)
import           Release (ReleaseItem (..))
import           Types

import           Data.Aeson
import qualified Data.Text.Lazy.IO as T
import           Data.Time
import           System.Directory (createDirectoryIfMissing)
import           System.IO (hPutStrLn, stderr)
import           Text.Mustache

renderPage :: (Show ctx,ToJSON ctx) => String -> PName -> FilePath -> FilePath -> Page ctx -> IO ()
renderPage tpls tpl dir file ctx = do
  tpl <- compileMustacheDir tpl tpls
  let (ws,t) = renderMustacheW tpl $ toJSON ctx
  forM_ ws $ hPutStrLn stderr . displayMustacheWarning
  createDirectoryIfMissing True dir
  T.writeFile (dir ++ "/" ++ file) t

renderWithPagination :: (Show ctx, ToJSON ctx)
                     => String
                     -> PName
                     -> FilePath
                     -> String
                     -> ([items] -> Page ctx)
                     -> Int
                     -> [items]
                     -> IO ()
renderWithPagination tpls tplName dir fname makeCtx count ctx = do
  tpl <- compileMustacheDir tplName tpls
  let ctxs = zip [0..] $ map makeCtx $ splitBy count ctx
      n = 2
      total = length ctxs
  forM_ ctxs $ \(cur, ctx) -> do
    let prev = dropWhile (<0) [cur-n .. cur-1]
        next = takeWhile (<total) [cur+1 .. cur+n]
        toPageName i = PageName
          { pnLink = if i==0 then Nothing else Just i
          , pnText = i+1
          }
        pagination = PageCtx
          { pcPrefix = fname
          , pcSuffix = ".html"
          , pcFirst = if 0 `elem` prev || cur == 0 then Nothing else Just $ toPageName 0
          , pcPrevious = map toPageName prev
          , pcCurrent = toPageName cur
          , pcNext = map toPageName next
          , pcLast = if (total-1) `elem` next || cur == total-1 then Nothing else Just $ toPageName $ total-1
          }
        page = ctx { pagePage = Just pagination }
        fnameCur = fname ++ (if cur == 0 then "" else show cur) ++ ".html"
    renderPage tpls tplName dir fnameCur page

splitBy :: Int -> [a] -> [[a]]
splitBy cnt lst
  | null lst = []
  | length lst <= cnt = [lst]
  | otherwise = let (begin,end) = splitAt cnt lst
                in begin : splitBy cnt end

-- renderPages :: String -> String -> [Day] -> [Item] -> IO ()
-- renderPages tpls date rels context = do
--   hPutStrLn stderr $ "Templates dir: " ++ tpls
--   log "Render releases" $ renderReleases tpls date rels
--   log "Render index" $ renderIndex tpls date context
--   forM_ context $ \item -> do
--     log ("Render team " ++ show (riId $ releaseItem item)) $ do
--       renderTeam tpls date item
--   -- renderTournament
--   hPutStrLn stderr "Finished preparing files"

